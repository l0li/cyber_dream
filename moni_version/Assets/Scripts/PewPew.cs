﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PewPew : MonoBehaviour
{
    // Boolean that controls if the shoot has been fired
    bool fired = false;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow)){
            transform.position = GameObject.Find("Car").transform.position;
            transform.Translate(0, 0.5f, 0);
            fired = true;
        }
        if(fired == true){
            if(transform.position.z == -50){
                transform.position = new Vector3(0, 5, 5);
                fired = false;
            }else{
                transform.Translate(0, -0.5f, 0);
            }
        }
    }

    void OnCollisionEnter (Collision push){
        transform.position = new Vector3(0, 5, 5);

        fired = false;
    }
}
