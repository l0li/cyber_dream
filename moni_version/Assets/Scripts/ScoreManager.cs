﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ScoreManager : MonoBehaviour
{
    public Text textField;

    private void Update(){
        textField.text = PersistentManagerScript.Instance.Score.ToString();
    }
}
