﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public void ResumeGame(){
    	SceneManager.LoadScene("Level_1");
    	Time.timeScale = 1;
    	// GameObject.Find("Enemy0").GetComponent<Rigidbody>().isKinematic = false;
    }
}
