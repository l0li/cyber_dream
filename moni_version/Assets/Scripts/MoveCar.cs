﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCar : MonoBehaviour
{
    Animator anim;
    public int score = 0;
    

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow)){
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("MoveCar")){
                anim.SetTrigger("MoveLeft");
            }
            else if (anim.GetCurrentAnimatorStateInfo(0).IsName("RightSide")){
                anim.SetTrigger("MoveCenter");
            }
        } 
        if(Input.GetKeyDown(KeyCode.RightArrow)){
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("MoveCar")){
                anim.SetTrigger("MoveRight");
            }
            else if (anim.GetCurrentAnimatorStateInfo(0).IsName("LeftSide")){
                anim.SetTrigger("MoveCenter");
            }
        } 
    }
}
