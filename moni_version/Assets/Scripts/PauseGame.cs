﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseGame : MonoBehaviour
{
    public void Pause(){
    	SceneManager.LoadScene("PauseMenu");
    	PersistentManagerScript.Instance.Pause = true;
    	PersistentManagerScript.Instance.Timer = GameObject.Find("Road").GetComponent<GameController>().timer;
    	// GameObject.Find("Enemy0").GetComponent<Rigidbody>().isKinematic = true;
    	Time.timeScale = 0;
    }
}
