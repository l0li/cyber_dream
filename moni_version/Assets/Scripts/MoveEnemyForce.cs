﻿using UnityEngine;

public class MoveEnemyForce : MonoBehaviour
{
    public Rigidbody rb;
    Animator anim;

    public float fowardForce;

    void Start()
    {
        anim = GetComponent<Animator>();
        if(gameObject.GetComponent<MoveEnemyForce>().fowardForce > 0){
            fowardForce = gameObject.GetComponent<MoveEnemyForce>().fowardForce;
        }else{
            fowardForce = 300f;
        }
    }

    void FixedUpdate()
    {
        if(transform.position.z < 0){
            rb.AddForce(0, 0, fowardForce * Time.deltaTime);
        }else{
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
        
    }
}
