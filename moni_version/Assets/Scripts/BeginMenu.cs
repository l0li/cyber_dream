﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BeginMenu : MonoBehaviour
{
    public void PlayGame(){
    	SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    	PersistentManagerScript.Instance.Pause = false;
    }

    public void QuitGame(){
    	SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
