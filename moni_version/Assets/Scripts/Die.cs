﻿using UnityEngine;

public class Die : MonoBehaviour
{
    public float Sx = 0;
    public float Sy = 0;
    public float Sz = 0;

    public Rigidbody rb;

    void OnCollisionEnter (Collision push){
        if (push.collider.name == "PewPew")
        {               
            PersistentManagerScript.Instance.Score += 10;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            transform.position = new Vector3 (Sx, Sy, Sz);
        }
        if(push.collider.name == "Car"){
            PersistentManagerScript.Instance.Score -= 10;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            transform.position = new Vector3 (Sx, Sy, Sz);
        }
    }
}
