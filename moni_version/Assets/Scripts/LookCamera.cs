﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookCamera : MonoBehaviour
{
    // Start is called before the first frame update
    float smooth = 1.4f;
    float tiltAngle = 60.0f;

    void Update()
    {
        
        transform.Rotate(Time.deltaTime * smooth , 0, 0, Space.World);
    }
}
