﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    // Number od Enemies that can be used 
    int enemyNumber = 0;

    // Counter to the lenght of the enemies list
    int count = 0;

    int fimDaMusica = 35;
    public float timer = 0.0f;

    // Array that contains the time and which lane the enemy should be placed
    // [time, lane] = lane 1 == left lane; lane 0 == middle lane; lane -1 == right lane;
    int[,] listEnemies = new int[,]{{5,1},{6,1},{7,1},{10,0},{15,-1},{20,-1},{25,1}};

    void Start(){
        if(PersistentManagerScript.Instance.Pause){
            // Debug.Log("hi");
            timer = PersistentManagerScript.Instance.Timer - Time.deltaTime;
            PersistentManagerScript.Instance.Pause = false;
        }else{
            timer = Time.deltaTime;
        }
    }

    void FixedUpdate()
    {   
        timer += Time.deltaTime;
        PersistentManagerScript.Instance.Timer = timer;
        if(count < (listEnemies.Length / 2)){
            if(timer >= listEnemies[count,0]){
                float lane = 2.5f * listEnemies[count,1];
                
                GameObject.Find(String.Concat("Enemy",enemyNumber.ToString())).transform.position = new Vector3(lane, 4.5f, -49);
                if(enemyNumber == 2){
                    enemyNumber = 0;
                }else{
                    enemyNumber++;
                }
                count++;
            }
        }
        if(timer > fimDaMusica){
            SceneManager.LoadScene("EndMenu");
        }

        if(Input.GetKeyDown(KeyCode.Space)){
            count = 0;
        }
    }
}
