﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRoad : MonoBehaviour
{
    public float scrollX = 0f;
    public float scrollY = 0.3f;

    void Update () {
      float OffsetX = Time.time * scrollX;
      float OffsetY = Time.time * scrollY;

      GetComponent<Renderer>().material.mainTextureOffset = new Vector2 (OffsetX, OffsetY);
    }
}